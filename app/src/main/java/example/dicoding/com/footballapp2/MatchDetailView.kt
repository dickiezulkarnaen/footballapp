package example.dicoding.com.footballapp2

interface MatchDetailView {
    fun onSuccessHomeTeamDetail(team : TeamModel)
    fun onFailureHomeTeamDetail(message : String)
    fun onSuccessAwayTeamDetail(team : TeamModel)
    fun onFailureAwayTeamDetail(message : String)
}