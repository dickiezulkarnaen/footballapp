package example.dicoding.com.footballapp2

import io.reactivex.Observable
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Query

interface Network {
    @GET("api/v1/json/1/eventspastleague.php?id=4328")
    fun getLastMatch() : Observable<ScheduleModel>

    @GET("api/v1/json/1/eventsnextleague.php?id=4328")
    fun getNextMatch() : Observable<ScheduleModel>

    @GET("api/v1/json/1/lookupteam.php?id")
    fun getTeamDetail(@Query("id")teamId : String) : Observable<TeamModel>

    object service {
        const val BASE_URL : String = "https://www.thesportsdb.com/"
        fun getData() : Network{
            val retrofit = Retrofit.Builder().baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
                    .build().create(Network::class.java)
            return retrofit
        }
    }
}