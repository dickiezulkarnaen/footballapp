package example.dicoding.com.footballapp2


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_next_match.*

class NextMatchFragment : Fragment(), MatchView {

    lateinit var presenter: MatchPresenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_next_match, container, false)
        presenter = MatchPresenter()
        presenter.matchPresenter()
        presenter.attachView(this)
        getNextMatch()
        return view
    }

    private fun getNextMatch() {
        presenter.getNextMatchData()
    }

    override fun onSuccessMatch(schedule: ScheduleModel) {
        val adapter = ScheduleAdapter(context, schedule)
        rv_next_match.adapter = adapter
        rv_next_match.layoutManager = LinearLayoutManager(context)
    }

    override fun onFailureMatch(message: String) {
        Log.d("ERROR", message)
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

}
