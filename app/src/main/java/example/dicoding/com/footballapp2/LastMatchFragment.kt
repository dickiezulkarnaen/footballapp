package example.dicoding.com.footballapp2


import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v7.widget.LinearLayoutManager
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import kotlinx.android.synthetic.main.fragment_last_match.*

class LastMatchFragment : Fragment(), MatchView {

    lateinit var presenter: MatchPresenter

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?,
                              savedInstanceState: Bundle?): View? {
        val view = inflater.inflate(R.layout.fragment_last_match, container, false)
        presenter = MatchPresenter()
        presenter.attachView(this)
        presenter.matchPresenter()
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        getLastMatch()
    }

    private fun getLastMatch() {
        presenter.getLastMatchData()
    }

    override fun onSuccessMatch(schedule: ScheduleModel) {
        val adapter = ScheduleAdapter(context, schedule)
        rv_last_match.adapter = adapter
        rv_last_match.layoutManager = LinearLayoutManager(context)
    }

    override fun onFailureMatch(message: String) {
        Log.d("ERROR", message)
        Toast.makeText(context, message, Toast.LENGTH_SHORT).show()
    }

}
