package example.dicoding.com.footballapp2

import android.content.Context
import android.content.Intent
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.google.gson.GsonBuilder
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_match.view.*

class ScheduleAdapter(val context: Context?,
                      val schedule : ScheduleModel) : RecyclerView.Adapter<ScheduleAdapter.ScheduleHolder>(){

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ScheduleHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.item_match, parent, false)
        return ScheduleHolder(view)
    }

    override fun getItemCount(): Int {
        return schedule.events.size
    }

    override fun onBindViewHolder(holder: ScheduleHolder, position: Int) {
        holder.bindView(schedule.events[position])
        holder.itemView.setOnClickListener(View.OnClickListener {
            var intent = Intent(context, MatchDetailActivity::class.java)
            intent?.putExtra("match", GsonBuilder().create().toJson(schedule.events[position]))
            context?.startActivity(intent)
        })
    }

    class ScheduleHolder(view : View) : RecyclerView.ViewHolder(view) {
        fun bindView(event: Event) {
            itemView.tv_date.text = event.dateEvent
            itemView.tv_club1.text = event.strHomeTeam
            itemView.tv_club2.text = event.strAwayTeam
            itemView.tv_score1.text = event.intHomeScore
            itemView.tv_score2.text = event.intAwayScore
        }
    }

}