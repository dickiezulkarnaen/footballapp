package example.dicoding.com.footballapp2

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import com.google.gson.GsonBuilder
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_match_detail.*

class MatchDetailActivity : AppCompatActivity(), MatchDetailView {

    lateinit var presenter: MatchDetailPresenter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_match_detail)
        presenter = MatchDetailPresenter()
        presenter.matchDetailPresenter()
        presenter.attachView(this)
        getMatchDetail()

    }

    private fun getMatchDetail() {
        val match = GsonBuilder().create().fromJson(intent.getStringExtra("match"), Event::class.java)

        presenter.getHomeTeamDetail(match.idHomeTeam)
        presenter.getAwayTeamDetail(match.idAwayTeam)
        Log.d("ID", match.idHomeTeam)

        tv_date_detail.text = match.dateEvent
        tv_club1_detail.text = match.strHomeTeam
        tv_club2_detail.text = match.strAwayTeam
        tv_score1_detail.text = match.intHomeScore
        tv_score2_detail.text = match.intAwayScore
        tv_goal1_detail.text = match.strHomeGoalDetails
        tv_goal2_detail.text = match.strAwayGoalDetails
        tv_shot1_detail.text = match.intHomeShots
        tv_shot2_detail.text = match.intAwayShots
        tv_keeper1_detail.text = match.strHomeLineupGoalkeeper
        tv_keeper2_detail.text = match.strAwayLineupGoalkeeper
        tv_defence1_detail.text = match.strHomeLineupDefense
        tv_defence2_detail.text = match.strAwayLineupDefense
        tv_midfield1_detail.text = match.strHomeLineupMidfield
        tv_midfield2_detail.text = match.strAwayLineupMidfield
        tv_forward1_detail.text = match.strHomeLineupForward
        tv_forward2_detail.text = match.strAwayLineupForward
        tv_subtitutes1_detail.text = match.strHomeLineupSubstitutes
        tv_subtitutes2_detail.text = match.strAwayLineupSubstitutes

    }

    override fun onSuccessHomeTeamDetail(team: TeamModel) {
        Picasso.get().load(team.teams[0].strTeamBadge).into(img_club1_detail)
        Log.d("PATH", team.teams[0].strTeamBadge)
    }

    override fun onFailureHomeTeamDetail(message: String) {
        Log.d("ERROR", message)
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }

    override fun onSuccessAwayTeamDetail(team: TeamModel) {
        Picasso.get().load(team.teams[0].strTeamBadge).into(img_club2_detail)
    }

    override fun onFailureAwayTeamDetail(message: String) {
        Log.d("ERROR", message)
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show()
    }
}
