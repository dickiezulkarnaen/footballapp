package example.dicoding.com.footballapp2

import android.view.View
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class MatchPresenter  {
    lateinit var network: Network
    lateinit var view: MatchView

    fun matchPresenter() {
        network = Network.service.getData()
    }

    fun attachView(view: MatchView) {
        this.view = view
    }

    //LAST MATCH SERVICE
    fun getLastMatchData() {
        val sub = network.getLastMatch()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                    this::onSuccessMatch,
                    this::onFailureMatch
                )
        CompositeDisposable().add(sub)
    }

    //NEXT MATCH SERVICE
    fun getNextMatchData() {
        val sub = network.getNextMatch()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        this::onSuccessMatch,
                        this::onFailureMatch
                )
        CompositeDisposable().add(sub)
    }

    private fun onSuccessMatch(schedule : ScheduleModel) {
        view.onSuccessMatch(schedule)
    }

    private fun onFailureMatch(t : Throwable) {
        view.onFailureMatch(t.localizedMessage)
    }



}