package example.dicoding.com.footballapp2

interface MatchView {
    fun onSuccessMatch(schedule : ScheduleModel)
    fun onFailureMatch(message : String)
}