package example.dicoding.com.footballapp2

import android.util.Log
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.schedulers.Schedulers

class MatchDetailPresenter {
    lateinit var network: Network
    lateinit var view: MatchDetailView

    fun matchDetailPresenter() {
        network = Network.service.getData()
    }

    fun attachView(view : MatchDetailView) {
        this.view = view
    }

    //========HOME DETAIL==============================
    fun getHomeTeamDetail(homeTeamId : String) {
        val sub = network.getTeamDetail(homeTeamId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        this::onSuccessHomeTeamDetail,
                        this::onFailureHomeTeamDetail
                )
        CompositeDisposable().add(sub)
    }

    fun onSuccessHomeTeamDetail(team: TeamModel) {
        view.onSuccessHomeTeamDetail(team)
    }

    fun onFailureHomeTeamDetail(t : Throwable) {
        view.onFailureHomeTeamDetail(t.localizedMessage)
    }



    //=========AWAY DETAIL=============================
    fun getAwayTeamDetail(awayTeamId : String) {
        val sub = network.getTeamDetail(awayTeamId)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe(
                        this::onSuccessAwayTeamDetail,
                        this::onFailureAwayTeamDetail
                )
        CompositeDisposable().add(sub)
    }

    fun onSuccessAwayTeamDetail(team: TeamModel) {
        view.onSuccessAwayTeamDetail(team)
    }

    fun onFailureAwayTeamDetail(t : Throwable) {
        view.onFailureAwayTeamDetail(t.localizedMessage)
    }
}